'use strict';

const utility = require('..');
const assert = require('assert').strict;

assert.strictEqual(utility(), 'Hello from utility');
console.info("utility tests passed");
